package models

import (
	"time"
	"trello_chatbot/domain/entities"
)

func FormatFindBydate(date time.Time) entities.FindByDate {
	var data entities.FindByDate
	loc, _ := time.LoadLocation("Asia/Bangkok")
	data.FromDate = time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, loc)
	data.ToDate = time.Date(date.Year(), date.Month(), date.Day()+1, 0, 0, 0, 0, loc)
	return data
}
