package models

import (
	"fmt"
	"time"
	"trello_chatbot/domain/entities"

	"github.com/adlio/trello"
	"github.com/line/line-bot-sdk-go/v7/linebot"
)

func FormatWebhook(client *trello.Client, IDModel string, Description string, CallbackURL string) *trello.Webhook {
	var webhook trello.Webhook
	webhook.IDModel = IDModel
	webhook.Description = Description
	webhook.CallbackURL = CallbackURL
	return &webhook
}

func FormatOnChange(data *trello.BoardWebhookRequest) entities.OnChange {
	var change entities.OnChange
	change.Action = data.Action
	change.ChangeTime = time.Now()
	change.BoardID = data.Model.ID
	return change
}

func FormatAddboard(BoardID string, BnID string, BoardName string, BoardURL string, LineID string) entities.Board {
	var board entities.Board
	board.BoardID = BoardID
	board.Consumer.BnID = BnID
	board.BoardName = BoardName
	board.BoardURL = BoardURL
	board.Consumer.LineID = LineID
	return board
}

func FormatBoardItem(board_name string, last_change string, board_id string) linebot.FlexContainer {
	jsonData := []byte(fmt.Sprintf(`
	{
		"type": "bubble",
		"size": "micro",
		"header": {
			"type": "box",
			"layout": "vertical",
			"contents": [
				{
					"type": "text",
					"text": "%s",
					"color": "#ffffff",
					"align": "start",
					"size": "sm",
					"weight": "bold",
					"gravity": "center"
				}
			],
			"backgroundColor": "#007AC0",
			"paddingTop": "19px",
			"paddingAll": "12px",
			"paddingBottom": "16px"
		},
		"body": {
			"type": "box",
			"layout": "vertical",
			"contents": [
				{
					"type": "text",
					"text": "Last Activity",
					"size": "sm",
					"color": "#666666"
				},
				{
					"type": "text",
					"text": "%s",
					"size": "xs",
					"color": "#666666"
				}
			],
			"spacing": "md",
			"paddingAll": "12px"
		},
		"footer": {
			"type": "box",
			"layout": "vertical",
			"contents": [
				{
					"type": "button",
					"action": {
						"type": "postback",
						"label": "Select",
						"data": "<!board_id|%s!>"
					  },
					"style": "primary",
					"height": "sm",
					"margin": "sm"
				}
			]
		},
		"styles": {
			"footer": {
				"separator": false
			}
		}
	}`, board_name, last_change, board_id))
	container, _ := linebot.UnmarshalFlexMessageJSON(jsonData)
	return container
}

func FormatChangeBoard(members []*trello.Member, OnChange []entities.OnChange) *entities.CarouselContainer {
	var payload entities.CarouselContainer
	for _, member := range members {
		member_task := FormatTaskCard(OnChange, member)
		payload.Contents = append(payload.Contents, FormatMemberCard(member, member_task))
	}
	payload.Type = "carousel"
	return &payload
}

func FormatMemberCard(member *trello.Member, task string) linebot.FlexContainer {
	membercard := fmt.Sprintf(`{
		"type": "bubble",
		"size": "giga",
		"hero": {
		"type": "box",
		"layout": "vertical",
		"contents": [
		   {
			  "type": "text",
			  "text": "%s",
			  "color": "#ffffff",
			  "weight": "bold",
			  "offsetStart": "47px",
			  "offsetTop": "7px"
		   },
		   {
			  "type": "box",
			  "layout": "vertical",
			  "contents": [
				 {
					"type": "text",
					"text": "%s"
				 }
			  ],
			  "position": "absolute",
			  "backgroundColor": "#eeeeee",
			  "cornerRadius": "xxl",
			  "paddingAll": "7px",
			  "offsetTop": "10px",
			  "offsetStart": "10px"
		   }
		],
		"backgroundColor": "#007AC0",
		"paddingAll": "12px",
		"paddingBottom": "26px"
		},
		"body": {
		   "type": "box",
		   "layout": "vertical",
		   "contents": [%s],
		   "spacing": "md",
		   "paddingAll": "12px"
		},
		"styles": {
		   "footer": {
			  "separator": false
		   }
		}
	}`, member.FullName, member.Username, task)
	container, _ := linebot.UnmarshalFlexMessageJSON([]byte(membercard))
	fmt.Println(membercard, container)
	return container
}

func FormatTaskCard(OnChanges []entities.OnChange, member *trello.Member) string {
	var perMember string
	loc, _ := time.LoadLocation("Asia/Bangkok")
	for _, onchange := range OnChanges {
		if onchange.Action.IDMemberCreator == member.ID {
			task := fmt.Sprintf(`{
				"type": "box",
				"layout": "vertical",
				"contents": [
				   {
					  "type": "text",
					  "text": "%s",
					  "size": "md",
					  "wrap": true,
					  "weight": "bold"
					},
					{
					  "type": "box",
					  "layout": "vertical",
					  "offsetStart": "20px",
					  "contents": [
							{
							"type": "text",
							"text": "card : %s",
							"size": "xs",
							"color": "#8C8C8C"
					  	}
					  ]
					},
					{
					  "type": "text",
					  "text": "%s",
					  "size": "xs",
					  "color": "#8C8C8C"
					},
					{
					  "type": "separator",
					  "margin": "md",
					  "color": "#007AC0"
					}
				],
				"flex": 1
			 }`, onchange.Action.Type, onchange.Action.Data.Card.Name, onchange.Action.Date.In(loc).Format("2006-01-02 15:04:05"))
			if perMember == "" {
				perMember = task
			}
			perMember += "," + task
		}
	}
	return perMember
}
