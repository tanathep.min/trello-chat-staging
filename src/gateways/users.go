package gateways

import (
	"encoding/json"
	"net/http"
	entities "trello_chatbot/domain/entities"

	"github.com/labstack/echo/v4"
)

func (h HTTPGateway) RegisterAsConsumer(c echo.Context) (err error) {
	user := new(entities.User)
	response_payload := new(entities.ResponseBody)
	if err := c.Bind(user); err != nil {
		return err
	}
	isMember, err := h.UserViewRecordService.RegisterAsConsumer(user)
	if err != nil {
		return c.String(400, err.Error())
	}
	c.Response().Header().Set("Response-Type", "object")
	c.Response().WriteHeader(http.StatusOK)
	if isMember {
		text := &entities.LineTextResponse{
			Type: "text",
			Text: "คุณได้ลงทะเบียนไว้แล้ว",
		}
		response_payload.LinePayload = append(response_payload.LinePayload, text)
		return json.NewEncoder(c.Response()).Encode(response_payload)
	} else {
		text := &entities.LineTextResponse{
			Type: "text",
			Text: "ลงทะเบียนเสร็จสิ้น",
		}
		response_payload.LinePayload = append(response_payload.LinePayload, text)
	}
	return json.NewEncoder(c.Response()).Encode(response_payload)
}
