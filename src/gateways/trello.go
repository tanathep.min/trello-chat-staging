package gateways

import (
	"encoding/json"
	"fmt"
	"net/http"
	"trello_chatbot/domain/entities"

	"github.com/adlio/trello"
	"github.com/labstack/echo/v4"
)

func (h HTTPGateway) Webhook(c echo.Context) (err error) {
	data, err := trello.GetBoardWebhookRequest(c.Request())
	h.TrelloViewRecordService.Webhook(data)
	return c.JSON(200, "ok")
}

func (h HTTPGateway) ConfirmWebhook(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "Success")
}

func (h HTTPGateway) CreateWebhook(c echo.Context) (err error) {
	user := new(entities.User)
	if err := c.Bind(user); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	h.TrelloViewRecordService.CreateWebhook(user.LineID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	// log.Println(webhook_data)
	// action := new(entities.TrelloAction)
	// if err := c.Bind(action); err != nil {
	// 	return err
	// }
	// action.ChangeTime = time.Now().Add(time.Hour * 72)
	return c.JSON(http.StatusOK, "ok")
}

func (h HTTPGateway) GetWebhook(c echo.Context) (err error) {
	user := new(entities.User)
	if err := c.Bind(user); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	res, err := h.TrelloViewRecordService.GetWebhook(user.LineID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	return c.JSON(http.StatusOK, res)
}

func (h HTTPGateway) DeleteWebhook(c echo.Context) (err error) {
	user := new(entities.User)
	if err := c.Bind(user); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	err = h.TrelloViewRecordService.DeleteWebhook(user.LineID)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, "ok")
}

func (h HTTPGateway) GetAllBoard(c echo.Context) (err error) {
	user := new(entities.User)
	if err := c.Bind(user); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	fmt.Println(user)
	res := h.TrelloViewRecordService.GetBoard(user.LineID)
	c.Response().Header().Set("Response-Type", "object")
	c.Response().WriteHeader(http.StatusOK)
	return json.NewEncoder(c.Response()).Encode(res)
}

func (h HTTPGateway) GetChangeByDate(c echo.Context) (err error) {
	// board := new(entities.Board)
	// if err := c.Bind(board); err != nil {
	// 	log.Println("board", err.Error())
	// 	return c.String(http.StatusBadRequest, err.Error())
	// }
	// user := new(entities.User)
	// if err := c.Bind(user); err != nil {
	// 	log.Println("user", err.Error())
	// 	return c.String(http.StatusBadRequest, err.Error())
	// }
	change_date := c.QueryParam("change_date")
	board_id := c.QueryParam("board_id")
	id := c.QueryParam("line_id")
	res, err := h.TrelloViewRecordService.GetChangeByDate(board_id, change_date, id)

	c.Response().Header().Set("Response-Type", "object")
	c.Response().WriteHeader(http.StatusOK)
	return json.NewEncoder(c.Response()).Encode(res)
}
