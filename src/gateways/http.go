package gateways

import (
	"os"
	sv "trello_chatbot/src/services"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type HTTPGateway struct {
	UserViewRecordService   sv.IUserService
	TrelloViewRecordService sv.ITrelloService
}

func NewHTTPGateway(e *echo.Echo, users sv.IUserService, trello_sv sv.ITrelloService) {
	gateway := &HTTPGateway{
		UserViewRecordService:   users,
		TrelloViewRecordService: trello_sv,
	}

	e.GET("/", Home)
	e.POST("/trello/webhook", gateway.Webhook)
	e.HEAD("/trello/webhook", gateway.ConfirmWebhook)

	user := e.Group("/user")
	user.GET("/register", gateway.RegisterAsConsumer)

	trello := e.Group("/trello")

	trello.Use(middleware.KeyAuthWithConfig(middleware.KeyAuthConfig{
		KeyLookup: "header:api-key",
		Validator: func(key string, c echo.Context) (bool, error) {
			return key == os.Getenv("API_KEY"), nil
		},
	}))

	trello.GET("/create_webhook", gateway.CreateWebhook)
	trello.GET("/get_webhook", gateway.GetWebhook)
	trello.GET("/delete_webhook", gateway.DeleteWebhook)

	trello.GET("/get_all_board", gateway.GetAllBoard)
	trello.GET("/get_change_by_date", gateway.GetChangeByDate)
}
