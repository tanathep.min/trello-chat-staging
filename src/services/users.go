package services

import (
	"trello_chatbot/domain/entities"
	"trello_chatbot/domain/repositories"
)

type Service struct {
	UserRepository repositories.IUserRecordRepository
}

type IUserService interface {
	RegisterAsConsumer(user *entities.User) (pass bool, err error)
}

func NewUserService(repo0 repositories.IUserRecordRepository) IUserService {
	return &Service{
		UserRepository: repo0,
	}
}

func (s Service) RegisterAsConsumer(user *entities.User) (isMember bool, err error) {
	_, found := s.UserRepository.GetUserByBotnoiID(user.BnID)
	if !found {
		res := s.UserRepository.UpdateUser(user)
		return found, res
	}
	return found, err
}
