package services

import (
	"fmt"
	"os"
	"time"
	"trello_chatbot/domain/entities"
	"trello_chatbot/domain/repositories"
	"trello_chatbot/src/models"
	"trello_chatbot/src/utils"

	"github.com/adlio/trello"
)

type TrelloService struct {
	TrelloRepository repositories.ITrelloRecordRepository
	UserRepository   repositories.IUserRecordRepository
}

type ITrelloService interface {
	CreateWebhook(id string) error
	Webhook(data *trello.BoardWebhookRequest)
	GetWebhook(id string) ([]*trello.Webhook, error)
	GetBoard(id string) *entities.ResponseBody
	DeleteWebhook(id string) error
	GetChangeByDate(board string, date string, id string) (response_payload entities.ResponseBody, err error)
}

func NewTrelloService(repo0 repositories.ITrelloRecordRepository, repo1 repositories.IUserRecordRepository) ITrelloService {
	return &TrelloService{
		TrelloRepository: repo0,
		UserRepository:   repo1,
	}
}

func (s TrelloService) CreateWebhook(id string) error {
	user, _ := s.UserRepository.GetUserByLineID(id)
	client := trello.NewClient(user.Trello.Key, user.Trello.Token)
	all_board, err := client.GetMyBoards()
	if err != nil {
		return err
	}
	for _, board := range all_board {
		webhook_data := models.FormatWebhook(client, board.ID, "Description", os.Getenv("DOMAIN_NAME")+"/trello/webhook")
		err = client.CreateWebhook(webhook_data)
		add_board := models.FormatAddboard(board.ID, user.BnID, board.Name, board.ShortURL, user.LineID)
		s.TrelloRepository.AddBoard(add_board)
	}
	return err
}

func (s TrelloService) Webhook(data *trello.BoardWebhookRequest) {
	change_details := models.FormatOnChange(data)
	s.TrelloRepository.AddChange(change_details)
	s.TrelloRepository.UpdateBoardLastChange(data.Model.ID)
}

func (s TrelloService) GetWebhook(id string) ([]*trello.Webhook, error) {
	var webhook_list []*trello.Webhook
	user, _ := s.UserRepository.GetUserByLineID(id)
	client := trello.NewClient(user.Trello.Key, user.Trello.Token)
	token, err := client.GetToken(user.Trello.Token)
	if err != nil {
		return webhook_list, err
	}
	webhook_list, err = token.GetWebhooks()
	return webhook_list, err
}

func (s TrelloService) GetBoard(id string) *entities.ResponseBody {
	response_payload := new(entities.ResponseBody)
	carousel := new(entities.CarouselContainer)
	flex := new(entities.FlexContainer)
	board_list := s.TrelloRepository.GetBoardByLineID(id)
	for _, element := range board_list {
		container := models.FormatBoardItem(element.BoardName, utils.FormatDateTime(element.LastChange).Format("02-Jan-2006 15:04"), element.BoardID)
		carousel.Contents = append(carousel.Contents, container)
		carousel.Type = "carousel"
	}
	flex.Type = "flex"
	flex.AltText = "Trello Board"

	flex.Contents = carousel
	response_payload.LinePayload = append(response_payload.LinePayload, flex)
	return response_payload
}

func (s TrelloService) DeleteWebhook(id string) error {
	user, _ := s.UserRepository.GetUserByLineID(id)
	client := trello.NewClient(user.Trello.Key, user.Trello.Token)
	token, err := client.GetToken(user.Trello.Token)
	webhook, err := token.GetWebhooks()
	for _, element := range webhook {
		element.Delete()
	}
	return err
}

func (s TrelloService) GetChangeByDate(board_id string, date string, id string) (response_payload entities.ResponseBody, err error) {
	flex := new(entities.FlexContainer)
	user, _ := s.UserRepository.GetUserByLineID(id)
	fmt.Println(user)
	client := trello.NewClient(user.Trello.Key, user.Trello.Token)
	trello_board, err := client.GetBoard(board_id)
	if err != nil {
		fmt.Println("board error :", err)
		return response_payload, err
	}
	member, err := trello_board.GetMembers()
	if err != nil {
		fmt.Println("member error :", err)
		return response_payload, err
	}
	t, err := time.Parse("2006-01-02T15:04", date)
	find_date := models.FormatFindBydate(t)
	all_change := s.TrelloRepository.GetChangeByBoardID(board_id, find_date)
	container := models.FormatChangeBoard(member, all_change)

	flex.Type = "flex"
	flex.AltText = "Trello Change Board"
	flex.Contents = container
	response_payload.LinePayload = append(response_payload.LinePayload, flex)
	if err != nil {
		fmt.Println(err)
	}
	return response_payload, err
}
