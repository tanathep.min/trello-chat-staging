package utils

import (
	"log"
	"time"
)

func GetTimeNow() time.Time {

	loc, _ := time.LoadLocation("Asia/Bangkok")
	timenow := time.Now().In(loc)
	updateTime := timenow.Format("2006-01-02 15:04:05")
	log.Println(updateTime)
	return timenow
}

func FormatDateTime(input time.Time) time.Time {
	loc, _ := time.LoadLocation("Asia/Bangkok")
	return input.In(loc)
}
