package main

import (
	"log"
	"net/http"
	"os"

	datasources "trello_chatbot/domain/datasources"
	"trello_chatbot/domain/repositories"
	gateway "trello_chatbot/src/gateways"
	sv "trello_chatbot/src/services"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func goDotEnvVariable() {
	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

}

func main() {
	e := echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())

	goDotEnvVariable()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))

	mongodb := datasources.NewMongoDB(100)

	users := repositories.NewUserRepository(mongodb)
	trello := repositories.NewTrelloRepository(mongodb)

	sv0 := sv.NewUserService(users)
	sv1 := sv.NewTrelloService(trello, users)

	gateway.NewHTTPGateway(e, sv0, sv1)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
