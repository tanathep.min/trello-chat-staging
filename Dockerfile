FROM golang:alpine as build-env
ENV CGO_ENABLED=1
ENV GO111MODULE=on
WORKDIR $GOPATH/src/openapi-crud

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build

CMD /go/src/openapi-crud/openapi-crud