# github.com/adlio/trello v1.9.0
## explicit
github.com/adlio/trello
# github.com/armon/go-radix v1.0.0
github.com/armon/go-radix
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
github.com/dgrijalva/jwt-go
# github.com/elastic/go-licenser v0.3.1
github.com/elastic/go-licenser
github.com/elastic/go-licenser/licensing
# github.com/elastic/go-sysinfo v1.7.0
## explicit
github.com/elastic/go-sysinfo
github.com/elastic/go-sysinfo/internal/registry
github.com/elastic/go-sysinfo/providers/aix
github.com/elastic/go-sysinfo/providers/darwin
github.com/elastic/go-sysinfo/providers/linux
github.com/elastic/go-sysinfo/providers/shared
github.com/elastic/go-sysinfo/providers/windows
github.com/elastic/go-sysinfo/types
# github.com/elastic/go-windows v1.0.1
## explicit
github.com/elastic/go-windows
# github.com/go-stack/stack v1.8.0
github.com/go-stack/stack
# github.com/golang/snappy v0.0.4
## explicit
github.com/golang/snappy
# github.com/joeshaw/multierror v0.0.0-20140124173710-69b34d4ec901
github.com/joeshaw/multierror
# github.com/joho/godotenv v1.3.0
## explicit
github.com/joho/godotenv
# github.com/klauspost/compress v1.13.1
## explicit
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/labstack/echo/v4 v4.4.0
## explicit
github.com/labstack/echo/v4
github.com/labstack/echo/v4/middleware
# github.com/labstack/gommon v0.3.0
github.com/labstack/gommon/bytes
github.com/labstack/gommon/color
github.com/labstack/gommon/log
github.com/labstack/gommon/random
# github.com/line/line-bot-sdk-go/v7 v7.10.0
## explicit
github.com/line/line-bot-sdk-go/v7/linebot
# github.com/mattn/go-colorable v0.1.8
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.13
## explicit
github.com/mattn/go-isatty
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/prometheus/procfs v0.7.0
## explicit
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# github.com/santhosh-tekuri/jsonschema v1.2.4
github.com/santhosh-tekuri/jsonschema
github.com/santhosh-tekuri/jsonschema/decoders
github.com/santhosh-tekuri/jsonschema/formats
github.com/santhosh-tekuri/jsonschema/loader
github.com/santhosh-tekuri/jsonschema/mediatypes
# github.com/valyala/bytebufferpool v1.0.0
github.com/valyala/bytebufferpool
# github.com/valyala/fasttemplate v1.2.1
github.com/valyala/fasttemplate
# github.com/xdg-go/pbkdf2 v1.0.0
github.com/xdg-go/pbkdf2
# github.com/xdg-go/scram v1.0.2
github.com/xdg-go/scram
# github.com/xdg-go/stringprep v1.0.2
github.com/xdg-go/stringprep
# github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a
## explicit
github.com/youmark/pkcs8
# go.elastic.co/apm v1.12.0
go.elastic.co/apm
go.elastic.co/apm/apmconfig
go.elastic.co/apm/internal/apmcloudutil
go.elastic.co/apm/internal/apmcontext
go.elastic.co/apm/internal/apmhostutil
go.elastic.co/apm/internal/apmhttputil
go.elastic.co/apm/internal/apmlog
go.elastic.co/apm/internal/apmschema
go.elastic.co/apm/internal/apmstrings
go.elastic.co/apm/internal/apmversion
go.elastic.co/apm/internal/configutil
go.elastic.co/apm/internal/iochan
go.elastic.co/apm/internal/pkgerrorsutil
go.elastic.co/apm/internal/ringbuffer
go.elastic.co/apm/internal/wildcard
go.elastic.co/apm/model
go.elastic.co/apm/stacktrace
go.elastic.co/apm/transport
# go.elastic.co/apm/module/apmmongo v1.12.0
## explicit
go.elastic.co/apm/module/apmmongo
# go.elastic.co/fastjson v1.1.0
go.elastic.co/fastjson
# go.mongodb.org/mongo-driver v1.6.0
## explicit
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/address
go.mongodb.org/mongo-driver/mongo/description
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/awsv4
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/uuid
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
## explicit
golang.org/x/crypto/acme
golang.org/x/crypto/acme/autocert
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/scrypt
# golang.org/x/lint v0.0.0-20210508222113-6edffad5e616
## explicit
golang.org/x/lint
golang.org/x/lint/golint
# golang.org/x/mod v0.4.2
golang.org/x/mod/module
golang.org/x/mod/semver
# golang.org/x/net v0.0.0-20210716203947-853a461950ff
## explicit
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/h2c
golang.org/x/net/http2/hpack
golang.org/x/net/idna
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
golang.org/x/sync/errgroup
golang.org/x/sync/semaphore
# golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
## explicit
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
golang.org/x/sys/windows/registry
# golang.org/x/text v0.3.6
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6
## explicit
golang.org/x/time/rate
# golang.org/x/tools v0.1.5
## explicit
golang.org/x/tools/cmd/goimports
golang.org/x/tools/go/ast/astutil
golang.org/x/tools/go/gcexportdata
golang.org/x/tools/go/internal/gcimporter
golang.org/x/tools/internal/event
golang.org/x/tools/internal/event/core
golang.org/x/tools/internal/event/keys
golang.org/x/tools/internal/event/label
golang.org/x/tools/internal/fastwalk
golang.org/x/tools/internal/gocommand
golang.org/x/tools/internal/gopathwalk
golang.org/x/tools/internal/imports
golang.org/x/tools/internal/typeparams
# golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
golang.org/x/xerrors
golang.org/x/xerrors/internal
# howett.net/plist v0.0.0-20201203080718-1454fab16a06
## explicit
howett.net/plist
