module trello_chatbot

// +heroku goVersion go1.16
go 1.16

require (
	github.com/adlio/trello v1.9.0
	github.com/elastic/go-sysinfo v1.7.0 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/labstack/echo/v4 v4.4.0
	github.com/line/line-bot-sdk-go/v7 v7.10.0
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/prometheus/procfs v0.7.0 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.elastic.co/apm/module/apmmongo v1.12.0
	go.mongodb.org/mongo-driver v1.6.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20210716203947-853a461950ff // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6 // indirect
	golang.org/x/tools v0.1.5 // indirect
	howett.net/plist v0.0.0-20201203080718-1454fab16a06 // indirect
)
