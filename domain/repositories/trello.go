package repositories

import (
	"context"
	"fmt"
	"log"
	"os"

	. "trello_chatbot/domain/datasources"
	"trello_chatbot/domain/entities"
	"trello_chatbot/src/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type TrelloRecordRepository struct {
	Context          context.Context
	ChangeCollection *mongo.Collection
	BoardCollection  *mongo.Collection
}

type ITrelloRecordRepository interface {
	AddChange(change entities.OnChange) error
	AddBoard(board entities.Board) error
	UpdateBoardLastChange(board_id string) error
	GetBoardByLineID(line_id string) []entities.Board
	GetChangeByBoardID(board_id string, date entities.FindByDate) []entities.OnChange
	GetBoardByBotnoiID(id string) []entities.Board
}

func NewTrelloRepository(mongodb *MongoDB) ITrelloRecordRepository {
	return &TrelloRecordRepository{
		Context:          mongodb.Context,
		ChangeCollection: mongodb.MongoDB.Database(os.Getenv("DATABASE_NAME")).Collection("change"),
		BoardCollection:  mongodb.MongoDB.Database(os.Getenv("DATABASE_NAME")).Collection("board"),
	}
}

func (repo TrelloRecordRepository) AddChange(change entities.OnChange) error {
	_, err := repo.ChangeCollection.InsertOne(repo.Context, change)
	return err
}

func (repo TrelloRecordRepository) GetChangeByBoardID(board_id string, date entities.FindByDate) []entities.OnChange {
	defaultResult := []entities.OnChange{}
	options := options.Find()
	cur, err := repo.ChangeCollection.Find(repo.Context, bson.M{"board_id": board_id,
		"change_time": bson.M{
			"$gt": date.FromDate,
			"$lt": date.ToDate,
		}},
		options)
	defer cur.Close(repo.Context)
	if err != nil {
		return defaultResult
	}

	pack := make([]entities.OnChange, 0)
	for cur.Next(repo.Context) {
		var item entities.OnChange

		err := cur.Decode(&item)
		if err != nil {
			continue
		}
		pack = append(pack, item)
	}
	return pack
}

func (repo TrelloRecordRepository) AddBoard(board entities.Board) error {
	update := bson.M{"$set": board}
	opts := options.Update().SetUpsert(true)
	_, err := repo.BoardCollection.UpdateOne(repo.Context, bson.M{"consumer.line_id": board.Consumer.LineID, "board_id": board.BoardID}, update, opts)
	return err
}

func (repo TrelloRecordRepository) UpdateBoardLastChange(board_id string) error {
	update := bson.M{"$set": bson.M{"last_change": utils.GetTimeNow()}}
	opts := options.Update()
	_, err := repo.BoardCollection.UpdateOne(repo.Context, bson.M{"board_id": board_id}, update, opts)
	if err != nil {
		log.Println(err)
	}
	return err
}

func (repo TrelloRecordRepository) GetBoardByLineID(line_id string) []entities.Board {
	defaultResult := []entities.Board{}
	options := options.Find()
	cur, err := repo.BoardCollection.Find(repo.Context, bson.M{"consumer.line_id": line_id}, options)
	defer cur.Close(repo.Context)
	if err != nil {
		return defaultResult
	}

	pack := make([]entities.Board, 0)
	for cur.Next(repo.Context) {
		var item entities.Board

		err := cur.Decode(&item)
		if err != nil {
			continue
		}
		pack = append(pack, item)
	}
	return pack
}

func (repo TrelloRecordRepository) GetBoardByBotnoiID(id string) []entities.Board {
	defaultResult := []entities.Board{}
	fmt.Println(id)
	options := options.Find()
	cur, err := repo.BoardCollection.Find(repo.Context, bson.M{"consumer.bn_id": id}, options)
	defer cur.Close(repo.Context)
	if err != nil {
		return defaultResult
	}

	pack := make([]entities.Board, 0)
	for cur.Next(repo.Context) {
		var item entities.Board

		err := cur.Decode(&item)
		if err != nil {
			continue
		}
		pack = append(pack, item)
	}
	return pack
}
