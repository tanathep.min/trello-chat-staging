package repositories

import (
	"context"
	"log"
	"os"

	. "trello_chatbot/domain/datasources"
	entities "trello_chatbot/domain/entities"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserRecordRepository struct {
	Context        context.Context
	UserCollection *mongo.Collection
}

type IUserRecordRepository interface {
	UpdateUser(user *entities.User) error
	GetUserByLineID(id string) (users entities.User, isMember bool)
	GetUserByBotnoiID(id string) (users entities.User, isMember bool)
}

func NewUserRepository(mongodb *MongoDB) IUserRecordRepository {
	return &UserRecordRepository{
		Context:        mongodb.Context,
		UserCollection: mongodb.MongoDB.Database(os.Getenv("DATABASE_NAME")).Collection("users"),
	}
}

func (repo UserRecordRepository) UpdateUser(user *entities.User) error {
	_, err := repo.UserCollection.InsertOne(repo.Context, user)
	return err
}

func (repo UserRecordRepository) GetUserByLineID(id string) (users entities.User, isMember bool) {
	var user entities.User
	if mongo_err := repo.UserCollection.FindOne(repo.Context, bson.M{"line_id": id}).Decode(&user); mongo_err != nil {
		if mongo_err == mongo.ErrNoDocuments {
			return user, false
		}
		log.Println(mongo_err)
	}
	return user, true
}

func (repo UserRecordRepository) GetUserByBotnoiID(id string) (users entities.User, isMember bool) {
	var user entities.User
	if mongo_err := repo.UserCollection.FindOne(repo.Context, bson.M{"bn_id": id}).Decode(&user); mongo_err != nil {
		if mongo_err == mongo.ErrNoDocuments {
			return user, false
		}
		log.Println(mongo_err)
	}
	return user, true
}
