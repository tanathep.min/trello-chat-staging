package datasources

import (
	"context"
	"log"
	"os"
	"time"

	"go.elastic.co/apm/module/apmmongo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDB struct {
	Context context.Context
	MongoDB *mongo.Client
}

type IMongoDB interface {
	Disconnect()
	Ping()
}

func NewMongoDB(maxPoolSize uint64) *MongoDB {
	option := options.Client().ApplyURI(os.Getenv("MONGODB_URI")).SetMonitor(apmmongo.CommandMonitor()).SetMaxPoolSize(maxPoolSize)
	client, err0 := mongo.Connect(context.Background(), option)

	ctx, _ := context.WithTimeout(context.Background(), 1000*time.Second)
	// defer cancel()

	if err0 != nil {
		log.Fatal("error connection : ", err0)
	}

	return &MongoDB{
		Context: ctx,
		MongoDB: client,
	}
}

func (ds MongoDB) Disconnect() {
	ds.MongoDB.Disconnect(ds.Context)
}

func (ds MongoDB) Ping() {

}
