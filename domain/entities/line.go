package entities

type FlexContainer struct {
	Type     string             `json:"type"`
	AltText  string             `json:"altText"`
	Contents *CarouselContainer `json:"contents"`
}

type CarouselContainer struct {
	Type     string        `json:"type"`
	Contents []interface{} `json:"contents"`
}
