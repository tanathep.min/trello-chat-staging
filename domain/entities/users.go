package entities

type User struct {
	LineID     string      `json:"line_id,omitempty" bson:"line_id" query:"line_id"`
	BnID       string      `json:"bn_id" bson:"bn_id" query:"bn_id"`
	Name       string      `json:"name" bson:"name" query:"name,omitempty"`
	ProfileImg string      `json:"profile_img_url" bson:"profile_img_url" query:"profile_img_url,omitempty"`
	Email      string      `json:"email" bson:"email" query:"email,omitempty"`
	Trello     TrelloToken `json:"trello" bson:"trello,omitempty"`
}

type TrelloToken struct {
	Token string `json:"trello_token,omitempty" bson:"trello_token" query:"trello_token"`
	Key   string `json:"trello_key,omitempty" bson:"trello_key" query:"trello_key"`
}

type Consumer struct {
	LineID string `json:"line_id,omitempty" bson:"line_id" query:"line_id"`
	BnID   string `json:"bn_id" bson:"bn_id" query:"bn_id"`
}
