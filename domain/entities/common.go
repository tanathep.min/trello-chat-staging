package entities

import "time"

type FindByDate struct {
	FromDate time.Time
	ToDate   time.Time
}
