package entities

import (
	"time"

	"github.com/adlio/trello"
)

type OnChange struct {
	Action     *trello.Action `json:"action" bson:"action"`
	ChangeTime time.Time      `json:"change_time" bson:"change_time"`
	BoardID    string         `json:"board_id,omitempty" bson:"board_id" query:"board_id"`
}

type Board struct {
	BoardID    string    `json:"board_id,omitempty" bson:"board_id" query:"board_id,omitempty"`
	BoardName  string    `json:"board_name,omitempty" bson:"board_name" query:"board_name,omitempty"`
	BoardURL   string    `json:"board_url,omitempty" bson:"board_url" query:"board_url,omitempty"`
	Consumer   Consumer  `json:"consumer,omitempty" bson:"consumer" query:"consumer,omitempty"`
	LastChange time.Time `json:"last_change,omitempty" bson:"last_change,omitempty,omitempty"`
}

type Member struct {
	ID   string `json:"member_id,omitempty" bson:"member_id" query:"member_id"`
	Name string `json:"member_name,omitempty" bson:"member_name" query:"member_name"`
}
