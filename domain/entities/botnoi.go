package entities

type ResponseBody struct {
	LinePayload     []interface{} `json:"line_payload"`
	FacebookPayload []interface{} `json:"facebook_payload"`
}

type LineTextResponse struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type FacebookTextResponse struct {
	Text string `json:"text"`
}
